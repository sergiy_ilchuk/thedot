#!/bin/bash

function myreadlink() {
  (
  cd $(dirname $1)         # or  cd ${1%/*}
  echo $PWD/$(basename $1) # or  echo $PWD/${1##*/}
  )
}

BASEDIR=$(dirname $(myreadlink "$0"))

aggregate_folder () {
    cd $BASEDIR/src/$1/

    rm ../../$1/cursors/*

    for f in *.conf; do
        echo "Conf file: $f"
        xcursorgen $f ../../$1/cursors/${f%.*}
    done

    cp -a $BASEDIR/src/links/* ../../$1/cursors/

    cd $BASEDIR/
}

aggregate_folder Dot-Dark
aggregate_folder Dot-Light

