# TheDOT

TheDOT is a Mouse Theme for gnome with circles. 
It doesn&#39;t have a right or left arrow.
Which can be unusual but ergonomic for someone.

The general structure of the config files is:
[size] [x_cordinate_cursor] [y_cordinate_cursor] [path_to_image_file] [refresh_task_of_xcursor]

This is a derivative of an unmaintained orginal TheDOT 
https://www.gnome-look.org/p/999763/

Dissasembled with https://github.com/eworm-de/xcur2png
(poor img quality, for images had to use https://github.com/alessandrofrancesconi/gimp-plugin-bimp)

Useful tutorial https://gist.github.com/sole/571812